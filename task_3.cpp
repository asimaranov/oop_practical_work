#include "iostream"
using namespace std;

// Variant 2

class A1{
    protected:
    int a1;

    public:
    A1(int a1){
        this->a1 = a1;
    }
    virtual void print(){
        cout << "A1" << endl;
    }
    virtual void show(){
        cout << "a1: " << a1 << endl;
    }
};


class B1 : public A1{
protected:
    int b1;

public:
    B1(int a1, int b1): A1(a1){
        this->b1 = b1;
    }
    void print() override{
        cout << "B1" << endl;
    }
    void show() override{
        cout << "a1: " << a1 << ", b1: " << b1 << endl;
    }
};



class C1 : public B1{
protected:
    int c1;

public:
    C1(int a1, int b1, int c1): B1(a1, b1){
        this->c1 = c1;
    }
    void print() override{
        cout << "C1" << endl;
    }
    void show() override{
        cout << "a1: " << a1 << ", b1: " << b1 << ", c1: " << c1 << endl;
    }
};

class C2 : public B1{
protected:
    int c2;

public:
    C2(int a1, int b1, int c2): B1(a1, b1){
        this->c2 = c2;
    }
    void print() override{
        cout << "C2" << endl;
    }
    void show() override{
        cout << "a1: " << a1 << ", b1: " << b1 << ", c2: " << c2 << endl;
    }
};

int main3()
{
    B1 test(1, 2);
    test.show();
    test.print();

    A1* ptr = &test;
    ptr->show();
    ptr->print();

    return 0;
}