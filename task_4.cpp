#include <iostream>

using namespace std;

class BaseString {
protected:
    char *p;
    int len;
    int capacity;
public:
    BaseString(char *ptr) {
        cout << "\nBase Constructor 1\n";
        len = strlen(ptr) + 1;
        capacity = 256;
        p = new char[capacity];
        for (int i = 0; i < len; i++) {
            p[i] = ptr[i];
        }
        p[len] = '\0';
    }

    BaseString(int Capacity = 256) {
        cout << "\nBase Constructor 0\n";
        capacity = Capacity;
        p = new char[capacity];
        len = 0;
    }

    ~BaseString() {
        cout << "\nBase Destructor\n";

        delete[] p;
        len = 0;
    }

    int Length() { return len; }

    int Capacity() { return capacity; }

//char* get() {return p;}
    char &operator[](int i) { return p[i]; }

    BaseString &operator=(BaseString &s) {
        cout << "\nBase Operator = \n";

        delete[] p;
        len = s.Length();
        p = new char[s.capacity];
        capacity = s.capacity;
        for (int i = 0; i < s.Length(); i++) {
            p[i] = s[i];
        }

        p[len - 1] = '\0';
        return *this;
    }

    BaseString(BaseString &s) {
        cout << "\nBase Copy Constructor\n";

        delete[] p;
        len = s.Length();
        p = new char[s.capacity];
        capacity = s.capacity;
        for (int i = 0; i < s.Length() - 1; i++) {
            p[i] = s[i];
        }
        p[len - 1] = '\0';
    }

    virtual void print() {
        int i = 0;
        while (p[i] != '\0') {
            cout << p[i];
            i++;
        }
        cout << endl;
    }
};

class String : public BaseString {
public:
    String(char *ptr) : BaseString(ptr) { cout << "\nDerived Constructor 1\n"; }

    String(int Capacity = 256) : BaseString(Capacity) {
        cout <<
             "\nDerived Constructor 0\n";
    }



    String &operator=(String &s) {
        cout << "\nDerived operator = \n";
        BaseString::operator=((BaseString &) s);
        return *this;
    }

    String(String &s) {

        delete[] p;
        len = s.Length();
        p = new char[s.capacity];
        capacity = s.capacity;
        for (int i = 0; i < s.Length() - 1; i++) {
            p[i] = s[i];
        }
        p[len - 1] = '\0';
    }

    String &operator+(String &s) {
        int len1 = len + s.Length() - 1;
        int l1 = len;
        int i = l1 - 1;
        while (s[i - l1] != '\0') {
            p[i] = s[i - l1 + 1];
            i++;
        }
        p[i] = '\0';
        len = len1;
        return *this;
    }

    int length_of_most_long_word(){
        int word_len = 0;
        int max_word_len = 0;
        for (int i=0; i < this->len -  1; i++){
            if(this->p[i] == ' '){
                if(word_len > max_word_len)
                    max_word_len = word_len;
                word_len = 0;

            }else
                word_len++;

        }

        if(word_len > max_word_len)
            max_word_len = word_len;

        return max_word_len;

    }
};

int main4() {
        String s("test ajkjbkjbkjbkjbk");
        s.print();
        cout << "Len: " << s.length_of_most_long_word() << endl;

    return 0;
}