Решение практических работ для сдачи долга Симаранова Андрея, КМБО-03-19,
27 вариант. В задачах где вариантов было меньше 27, считал номер варианта в остатке от деления на количество вариантов

Обнаруженные по ходу решения проблемы: 
- В 3 задаче код 
  ```c++
  B1 test(1, 2);
  test.show();
  test.print();

  A1* ptr = &test;
  ptr->show();
  ptr->print();
  ```
  
  Выводит 
  
  ```
  a1: 1, b1: 2
  B1
  a1: 1, b1: 2
  B1
  ```
  как и должен, так как ссылка на vtable создается конструктором потомка, и остается старой при касте в указатель на родителя. Мне показалось, что задумывалось другое поведение
  
- В 5 задаче в примере использовалась функция из C strcpy_s, которой нет в с++. Заменил ее на strncpy
- В 5 задаче исключение ловилось по значению, `catch (Exception e) {`. На это и линтер ругается, и таким образом не вызывалась перегруженная виртуальная функция родителя (https://stackoverflow.com/questions/28060941/about-an-exceptions-virtual-function-in-c). Заменил на ```catch (Exception &e) {```