#include <iostream>
#include <fstream>
#include <typeinfo>
#include <cstring>

using namespace std;

// strcpy_s doesn't exists in c++, so I replaced it with strncpy

class Exception : public std::exception {
protected:
//сообщение об ошибке
    char *str;
public:
    Exception(const char *s) {
        str = new char[strlen(s) + 1];
        strncpy(str, s, strlen(s) + 1);
    }

    Exception(const Exception &e) {
        str = new char[strlen(e.str) + 1];
        strncpy(str, e.str, strlen(e.str) + 1);
    }

    ~Exception() {
        delete[] str;
    }


    virtual void print() {
        cout << "Exception: " << str;
    }
};

class IndexOutOfBoundsException : public Exception {
public:
    IndexOutOfBoundsException(const char *s) : Exception(s) {};

    IndexOutOfBoundsException(const IndexOutOfBoundsException &e) noexcept : Exception("") {
        str = new char[strlen(e.str) + 1];
        strncpy(str, e.str, strlen(e.str) + 1);
    };

    void print() override {
        cout << "IndexOutOfBoundsException: " << str;

    };

};

class WrongDimensionException : public Exception {
public:
    WrongDimensionException(const char *s) : Exception(s) {};

    WrongDimensionException(const WrongDimensionException &e) noexcept : Exception("") {
        str = new char[strlen(e.str) + 1];
        strncpy(str, e.str, strlen(e.str) + 1);
    };

    void print() override {
        cout << "WrongDimensionException: " << str;

    };

};

class WrongSizeException : public WrongDimensionException { ;
public:
    WrongSizeException(const WrongSizeException &e) noexcept : WrongDimensionException("") {
        str = new char[strlen(e.str) + 1];
        strncpy(str, e.str, strlen(e.str) + 1);
    };

    void print() override {
        cout << "WrongDimensionException: " << str;

    };


    WrongSizeException(const char *s) : WrongDimensionException(s) {}
};

template<typename T>
class BaseMatrix {
protected:
    T **ptr;
    int height;
    int width;
public:
    BaseMatrix(int Height = 2, int Width = 2) {
        int *a;
        if (Height <= 0 || Width <= 0)
            throw WrongSizeException("Non-positive size of matrix");
        height = Height;
        width = Width;
        ptr = new T *[height];
        for (int i = 0; i < height; i++)
            ptr[i] = new T[width];
    }

    BaseMatrix(const BaseMatrix &M) {
        height = M.height;
        width = M.width;
        ptr = new T *[height];
        for (int i = 0; i < height; i++) {
            ptr[i] = new T[width];
            for (int j = 0; j < width; j++)
                ptr[i][j] = M.ptr[i][j];
        }
    }

    virtual ~BaseMatrix() {
        cout << "Base matrix destructor" << endl;
        if (ptr != nullptr) {
            for (int i = 0; i < height; i++)
                delete[] ptr[i];
            delete[] ptr;
            ptr = nullptr;
        }
    }

    void print() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++)
                cout << ptr[i][j] << " ";
            cout << "\n";
        }
    }

    double &operator()(int row, int column) {
        if (row < 0 || column < 0 || row >= height || column >= width)
            throw IndexOutOfBoundsException("Index is out of bounds");
        return ptr[row][column];
    }

    friend ostream &operator<<(ostream &ustream, BaseMatrix<double>
    obj);

    friend istream &operator>>(istream &ustream, BaseMatrix<double> &
    obj);
};

ostream &operator<<(ostream &ustream, BaseMatrix<double> obj) {
//ustream<<my_manip;
    if (typeid(ustream).name() == typeid(ofstream).name()) {
        ustream << obj.height << " " << obj.width << "\n";
        for (int i = 0; i < obj.height; i++) {
            for (int j = 0; j < obj.width; j++)
                ustream << obj.ptr[i][j] << "\n";
        }
        return ustream;
    }
    for (int i = 0; i < obj.height; i++) {
        for (int j = 0; j < obj.width; j++)
            ustream << obj.ptr[i][j] << " ";
        ustream << "\n";
    }
    return ustream;
}

istream &operator>>(istream &ustream, BaseMatrix<double> &obj) {
    if (typeid(ustream) == typeid(ifstream))
        ustream >> obj.height >> obj.width;
    for (int i = 0; i < obj.height; i++)
        for (int j = 0; j < obj.width; j++)
            ustream >> obj.ptr[i][j];
    return ustream;
}

ostream &my_manip(ostream &s) {
    s.precision(4);
    s.fill('%');
    s.width(10);
    return s;
}

class Matrix : public BaseMatrix<double> {
public:
    Matrix(int height, int width) : BaseMatrix(height, width) {};

    Matrix(const Matrix &M) {
        height = M.height;
        width = M.width;
        ptr = new double *[height];
        for (int i = 0; i < height; i++) {
            ptr[i] = new double[width];
            for (int j = 0; j < width; j++)
                ptr[i][j] = M.ptr[i][j];
        }
    }

    static Matrix pseudo_random_filled(int h, int w, int seed = 0) {
        Matrix m = Matrix(h, w);
        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
                m.ptr[i][j] = (i + j + seed) * (i - j - seed) % 255;
        return m;

    }

    Matrix do_strange_rotation() {
        Matrix m = Matrix(*this);
        for (int i = 0; i < this->height; i++) {
            int offset = 0;
            for (int j = 0; j < this->width; j++)
                if (ptr[i][j] < 0)
                    offset++;


            for (int j = 0; j < offset; j++) {
                m.ptr[i][j] = this->ptr[i][this->width - offset + j];
            }
            for (int j = 0; j < this->width - offset; j++) {
                m.ptr[i][j + offset] = this->ptr[i][j];
            }

        }

        return m;
    }

    Matrix &operator=(Matrix &M) noexcept { // copy =

        for (int i = 0; i < height; i++)
            delete[] ptr[i];
        delete[] ptr;

        height = M.height;
        width = M.width;


        ptr = new double *[height];

        for (int i = 0; i < height; i++) {
            ptr[i] = new double[width];
            for (int j = 0; j < width; j++)
                ptr[i][j] = M.ptr[i][j];
        }
        return *this;
    }
    Matrix &operator=(Matrix &&M) noexcept {  // move =

        for (int i = 0; i < height; i++)
            delete[] ptr[i];
        delete[] ptr;

        height = M.height;
        width = M.width;

        this->ptr = M.ptr;
        M.ptr = nullptr;

        return *this;
    }

    Matrix(const string& file_name = "out.txt"){
        ifstream fin(file_name);
        if (fin) {
            fin >> *this;
            fin.close();
        }
    }

    void save_to_file(const string& file_name = "out.txt"){
        ofstream fout(file_name);
        if (fout.is_open()) {
            fout << *this;
            fout.close();
        }
    }

};

int main5() {
    //auto m = Matrix::pseudo_random_filled(10, 10);
    //m.print();

    //m.do_strange_rotation().print();
    try {
        BaseMatrix<double> Wrong(-2, 0);

    }
    catch (Exception &e) {
        cout << "Exception has been caught: ";
        e.print();
    }
    cout << "\n";
    Matrix M = Matrix(2, 2);
    cin >> M;
    cout << "Original matrix: " << endl << M << endl;

    M = M.do_strange_rotation();


    cout << "Modified matrix: " << endl << M << endl;
    M.save_to_file("out.txt");

    M = Matrix("out.txt");

    cout << "Matrix from file:" << endl << M << endl;


    return 0;
}