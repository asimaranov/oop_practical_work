#include <iostream>
using namespace std;

class Matrix2x2{
private:
    double a_00, a_01, a_10, a_11;
public:
    Matrix2x2(double a_00, double a_01, double a_10, double a_11){
        this->a_00 = a_00;
        this->a_01 = a_01;
        this->a_10 = a_10;
        this->a_11 = a_11;
    }

    double operator -() const{
        return a_00 * a_11 - a_01 * a_10;
    }
    bool operator <(Matrix2x2 another) const{
        return -*this < -another;
    }
};

int main1(){
    Matrix2x2 m1 = Matrix2x2(1, 0, 0, 1);
    Matrix2x2 m2 = Matrix2x2(2, 0, 0, 2);

    cout << -m1 << endl;
    cout << -m2 << endl;
    cout << (m1 < m2 ? "true" : "false");
}