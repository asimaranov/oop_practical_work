#include <iostream>
#include <cmath>
#include <cstdlib>


using namespace std;
const double EPSILON = 2.22045e-016;

class MyArrayParent {
protected:
    int capacity;
    int count;
    double *ptr;
public:

    MyArrayParent(int Dimension = 100) {
        cout << "\nMyArray constructor";
        ptr = new double[Dimension];
        capacity = Dimension;
        count = 0;
    }

//конструктор принимает существующий массив
    MyArrayParent(double *arr, int len) : MyArrayParent(len) {
        cout << "\nMyArray constructor";
        for (int i = 0; i < len; i++) {
            this->ptr[i] = arr[i];
        }
    }

//деструктор
    ~MyArrayParent() {
        cout << "\nMyArray destructor";
        if (ptr != NULL) {
            delete[] ptr;
            ptr = NULL;
        }
    }

//обращение к полям
    int Capacity() { return capacity; }

    int Size() { return count; }

    double GetComponent(int index) {
        if (index >= 0 && index < count)
            return ptr[index];
        else throw std::out_of_range("Index out of range");

    }

    void SetComponent(int index, double value) {
        if (index >= 0 && index < count)
            ptr[index] = value;
        else throw std::out_of_range("Index out of range");

    }


    virtual void push(double value) {
        if (count < capacity) {
            ptr[count] = value;
            count++;
        } else throw std::runtime_error("Capacity limit exceeded");

    }

//удаление элемента с конца
    void RemoveLastValue() {
        if (count >= 0)
            count--;
        else throw std::runtime_error("Unable to remove: array is empty");

    }

    double &operator[](int index) {
        if (index < this->count) {
            return this->ptr[index];
        } else throw std::invalid_argument("No such index");

    }

    MyArrayParent &operator=(const MyArrayParent &V) {
        cout << "\noperator = ";
        this->capacity = V.capacity;

        delete[]this->ptr;
        this->ptr = new double[this->capacity];
        this->count = 0;
        for (int i = 0; i < V.count; i++) {
            push(V.ptr[i]);
        }
        return *this;

    }

    MyArrayParent(const MyArrayParent &V) : MyArrayParent(V.capacity) {
        cout << "\nCopy constructor";

        for (int i = 0; i < V.count; i++) {
            push(V.ptr[i]);
        }

//создание копии объекта - в основном, при возвращении результата из функции / передаче параметров в функцию
    }

    void print() {
        cout << "\nMyArr, size: " << count << ", values: {";
        int i = 0;
        for (i = 0; i < count; i++) {
            cout << ptr[i];
            if (i != count - 1)
                cout << ", ";
        }
        cout << "}" << endl;
    }
};

class MyArrayChild : public MyArrayParent {
public:

    MyArrayChild(int Dimension = 100) : MyArrayParent(Dimension) {
        cout <<
             "\nMyArrayChild constructor";
    }

    ~MyArrayChild() { cout << "\nMyArrayChild destructor\n"; }

//удаление элемента
    void RemoveAt(int index = -1) {
        if (index >= 0 && index < count) {
            count--;

            for (int i = index; i < count; i++) {
                this->ptr[i] = this->ptr[i + 1];
            }
        } else throw std::invalid_argument("Index out of range");

    };

//поиск элемента
    int IndexOf(double value, bool bFindFromStart = true) {
        for (int i = 0; i < count; i++) {

            if (fabs(ptr[i] - value) < EPSILON) {
                return i;
            }
        }
        return -1;
    };

//вставка элемента
    void InsertAt(double value, int index = -1) {
        if (index >= 0 && index <= count) {

            double *arr = new double[this->count + 1];
            for (int i = 0; i < index; i++) {
                arr[i] = this->ptr[i];
            }
            arr[index] = value;

            for (int i = index; i < this->count; i++) {
                arr[i + 1] = this->ptr[i];
            }
            delete[]this->ptr;
            this->ptr = arr;
            this->count++;

        }
    };

    virtual MyArrayChild MirrorItemsBetweenNegativeValues() {
        MyArrayChild copy = MyArrayChild(*this);
        int lastNegativeIndex = -1;
        for (int i = 0; i < count; i++) {  // 1 2 3  -1 (last negative)  4 5 6 7 8 9  -2 (i)  6 5 4 3
            if (copy.ptr[i] < 0) {
                if (lastNegativeIndex > -1) { // We're between two negative numbers
                    for (int j = 0; j < (i - lastNegativeIndex) / 2; j++) {  // Lets
                        double tmp_value = copy.ptr[i - j - 1];
                        copy.ptr[i - j - 1] = copy.ptr[lastNegativeIndex + 1 + j];
                        copy.ptr[lastNegativeIndex + 1 + j] = tmp_value;

                    }
                }

                lastNegativeIndex = i;

            }
        }
        return copy;
    }


};

class MySortedArray : public MyArrayChild {

public:
    MySortedArray(int Dimension = 100) : MyArrayChild(Dimension) {}

    void push(double value) override {
        if (this->count == 0) {
            this->ptr[0] = value;
            this->count++;
            return;
        }

        int left = 0;
        int right = this->count - 1;

        while (right - left > 1) {
            int center = left + (right - left) / 2;
            if (fabs(this->ptr[center] - value) < EPSILON) {
                left = right = center;
                break;
            }


            if (this->ptr[center] < value) {
                left = center + 1;
            } else {
                right = center - 1;
            }
        }

        if (fabs(this->ptr[left] - value) < EPSILON) {
            this->InsertAt(value, left + 1);
        } else if (fabs(this->ptr[right] - value) < EPSILON) {
            this->InsertAt(value, right + 1);
        } else if (value < left) {
            this->InsertAt(value, left);
        } else if (value > right and value < left) {
            this->InsertAt(value, right);

        } else if (value > right) {
            this->InsertAt(value, right + 1);
        }

    }

    MyArrayChild MirrorItemsBetweenNegativeValues() override {  // I have no idea how to change it
        return MyArrayChild::MirrorItemsBetweenNegativeValues();
    }


};

int main() {
    MyArrayChild arr(100);

    for (int i = 0; i < 10; i++) {
        arr.push(i + 1);
    }
    arr.print();
    MyArrayChild p1;
    p1 = arr;
    cout << "Original array: ";
    p1.print();


    p1.InsertAt(-1, 1);

    p1.InsertAt(-3, 5);

    p1.InsertAt(-4, 9);
    cout << "Array after insertions:" << endl;

    p1.print();
    cout << "Array after mirroring:" << endl;

    p1.MirrorItemsBetweenNegativeValues().print();

    MySortedArray sarr = MySortedArray(100);
    for (int i = 0; i < 10; i++) {
        sarr.push(i + 1);

    }
    sarr.push(11);
    sarr.push(6);
    sarr.push(-5);
    sarr.push(2.3);

    sarr.print(); // MyArr, size: 14, values: {-5, 1, 2, 2.3, 3, 4, 5, 6, 6, 7, 8, 9, 10, 11}

    return 0;

}